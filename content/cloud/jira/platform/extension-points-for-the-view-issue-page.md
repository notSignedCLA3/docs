---
title: Extension points for the View issue page
platform: cloud
product: jiracloud
category: reference
subcategory: modules 
aliases:
- /jiracloud/jira-platform-modules-view-issue-page-39988380.html
- /jiracloud/jira-platform-modules-view-issue-page-39988380.md
confluence_id: 39988380
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988380
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988380
date: "2016-09-30"
---
# Extension points for the 'View Issue' page

This pages lists the extension points for modules on the 'View issue' page in JIRA.

## Issue actions locations

Issue actions, such as sharing an issue, exporting an issue, and more, are implemented via buttons at the top right of the 'View issue' page. You can extend issue actions either by adding a new web item next to the issue actions, or by inserting a new web item in the more actions (i.e. ellipsis) menu.

#### Module type
`webItem`

#### Screenshot
<img src="../images/jdev-jiraissuetoolsops.png"/> 

#### Sample JSON
``` json
...
"modules": {
    "webItems": [
        {
            "key": "example-tools-item",
            "location": "operations-operations",
            "weight": 10,
            "name": {
                "value": "Example add-on link"
            },
            "url": "/example-section-link"
        }
    ]
}
...
```

#### Properties

See [Web item](/cloud/jira/platform/modules/web-item) for descriptions of a web item's properties. In addition, note the following information that is specific to this module:

`location`

-   **Description**: Set the location to one of the following:
    - `jira.issue.tools`: This location is next to the existing issue actions. 
    - `operations-top-level`, `operations-work`, `operations-attachments`, `operations-voteswatchers`, `operations-subtasks`, `operations-operations`, `operations-delete`: These locations are in the more actions (ellipsis) menu (see the screenshot above).

----

## Left-side of the 'View Issue' page location

Defines web panels for the left hand side of the 'View Issue' page, i.e. the main column, as shown in the screenshot below.

#### Module type
`webPanel`

#### Screenshot
<img src="../images/jdev-viewissueleft-location.png"/> 

#### Sample JSON
``` json
...
"modules": {
    "webPanels": [
        {
            "key": "example-issue-left-panel",
            "location": "atl.jira.view.issue.left.context",
            "name": {
                "value": "Example issue left panel"
            },
            "url": "/example-issue-left-panel"            
        }
    ]   
}
...
```

#### Properties

See [Web panel](/cloud/jira/platform/modules/web-panel) for descriptions of a web panel's properties. In addition, note the following information that is specific to this module:

`location`

-   **Description**: Set the location to `atl.jira.view.issue.left.context`.

  [i18n property]: /cloud/jira/platform/connect/modules/i18n-property
  [additional context]: /cloud/jira/platform/context-parameters

----

## Right-side of the 'View Issue' page location

Defines web panels for the right hand side of the 'View Issue' page, as shown in the screenshot below.

#### Module type
`webPanel`

#### Screenshot
<img src="../images/jdev-viewissueright-location.png"/> 

#### Sample JSON
``` json
...
"modules": {
    "webPanels": [
        {
            "key": "example-issue-right-panel",
            "location": "atl.jira.view.issue.right.context",
            "name": {
                "value": "Example issue right panel"
            },
            "url": "/example-issue-right-panel"            
        }
    ]   
}
...
```

#### Properties

See [Web panel](/cloud/jira/platform/modules/web-panel) for descriptions of a web panel's properties. In addition, note the following information that is specific to this module:

`location`

-   **Description**: Set the location to `atl.jira.view.issue.right.context`.

  [i18n property]: /cloud/jira/platform/connect/modules/i18n-property
  [additional context]: /cloud/jira/platform/context-parameters
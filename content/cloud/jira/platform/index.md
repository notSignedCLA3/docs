---
title: Latest updates
platform: cloud
product: jiracloud
category: devguide
subcategory: index
aliases:
- /jiracloud/latest-updates-39988013.html
- /jiracloud/latest-updates-39988013.md
confluence_id: 39988013
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988013
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988013
date: "2016-10-10"
---

# Latest updates

We deploy updates to JIRA Cloud frequently. As a JIRA developer, it's important that you're aware of the changes. The resources below will help you keep track of what's happening. 

## Atlassian Developer blog

Major changes that affect JIRA Cloud developers are announced in the **Atlassian Developer blog**, like new JIRA modules or the deprecation of API end points. You'll also find handy tips and articles related to JIRA development.

Check it out and subscribe here: [Atlassian Developer blog](https://developer.atlassian.com/blog/categories/jira/) *(JIRA-related posts)*

#### Recent announcements

Changes announced in the Atlassian Developer blog are usually described in more detail in this documentation. The most recent announcements are documented in detail below:

-   [Deprecation notice - toString representation of sprints in Get issue response](/cloud/jira/platform/deprecation-notice-tostring-representation-of-sprints-in-get-issue-response/)
-   [Change notice - changelogs are now limited to the 100 most recent items](/cloud/jira/platform/change-notice-changelogs-are-now-limited-to-the-100-most-recent-items/)
-   [Deprecation notice - removal of comment object data in jira](/cloud/jira/platform/deprecation-notice-removal-of-comment-object-data-in-jira-issue-webhooks/)
-   [Deprecation notice - removal of worklog object data in jira](/cloud/jira/platform/deprecation-notice-worklog-data-in-issue-related-events-for-webhooks/)

## What's new blog

Major changes that affect all users of the JIRA Cloud products are announced in the *What's New blog* for Atlassian Cloud. This includes new features, bug fixes, and other changes. For example, the introduction of a new JIRA quick search or a change in project navigation. 

Check it out and subscribe here: [What's new blog](https://confluence.atlassian.com/display/Cloud/What%27s+New) *(Note, this blog also includes changes to other Cloud applications)*



---
title: Basic auth for REST APIs
platform: cloud
product: jswcloud
category: devguide
subcategory: security
date: "2016-11-02"
---
{{< reuse-page path="docs/content/cloud/jira/platform/jira-rest-api-basic-authentication.md">}}
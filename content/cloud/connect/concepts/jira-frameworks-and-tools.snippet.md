# Frameworks and tools

Atlassian Connect add-ons can be written using many different languages, frameworks and tools. Since Atlassian Connect 
add-ons operate remotely over HTTP and can be written with any programming language and web framework there are many
tools available for you to develop your add-ons.


## Atlassian frameworks

We've written two different frameworks to help you get started. These frameworks help to generate some of the plumbing
required for your Connect add-on, and are officially supported by Atlassian:

 * [Atlassian Connect for Node.js Express](https://bitbucket.org/atlassian/atlassian-connect-express)
 * [Atlassian Connect for Spring Boot](https://bitbucket.org/atlassian/atlassian-connect-spring-boot/overview)
  
#### Additional options

Atlassian and our excellent developer community have also written a number of other frameworks that you can use. These frameworks are not supported by Atlassian but they may be supported by members of the community:

 * [Play (Java)](https://bitbucket.org/atlassian/atlassian-connect-play-java)
 * [Play (Scala)](https://bitbucket.org/atlassianlabs/atlassian-connect-play-scala)
 * [.NET](https://bitbucket.org/atlassianlabs/atlassian-connect-.net)
 * [Symfony2 Atlassian Connect Bundle (PHP)](https://github.com/thecatontheflat/atlassian-connect-bundle)
 * Dart
   * Atlassian Connect JWT library - [atlassian_connect_jwt](https://pub.dartlang.org/packages/atlassian_connect_jwt)
   * Services for handling communications with the host product - [atlassian_connect_host](https://pub.dartlang.org/packages/atlassian_connect_host)
   * Helpers for managing environment configuration - [atlassian_connect_config](https://pub.dartlang.org/packages/atlassian_connect_config)
   * Simple web server bundling the above components - [atlassian_connect_shelf](https://pub.dartlang.org/packages/atlassian_connect_shelf)
 * Haskell
   * [atlassian-connect-core](http://hackage.haskell.org/package/atlassian-connect-core)
   * [atlassian-connect-descriptor](http://hackage.haskell.org/package/atlassian-connect-descriptor)

## Developer Tools

**[JSON descriptor validator](https://atlassian-connect-validator.herokuapp.com/validate)**
This validator will check that your descriptor is syntactically correct. Paste the JSON content of your descriptor in the "descriptor" field, and select the Atlassian product you want to validate. 
	
**[JWT decoder](http://jwt-decoder.herokuapp.com/jwt/decode)**
An encoded JWT token can be opaque. Use this handy tool to decode JWT tokens and inspect their content. Just paste the full URL of the resource you are trying to access in the URL field, including the JWT token. For example, `https://example.atlassian.net/path/to/rest/endpoint?jwt=token`

**[Entity property tool](https://marketplace.atlassian.com/plugins/com.atlassian.connect.entity-property-tool.prod)**
In JIRA, you can store data against the host product without a backend. (Learn about [storing data without a database.](#storing-data-without-a-database)) This kind of hosted data storage is implemented via Entity Properties and the Entity Property tool making it trivial to create, read, update, and delete Entity Properties in JIRA. 
	   
**[JIRA web fragment finder](https://marketplace.atlassian.com/plugins/com.wittified.webfragment-finder) and [Confluence web fragment finder](https://marketplace.atlassian.com/plugins/com.wittified.webfragment-finder-confluence) ** 
The web fragment finder is an add-on which loads a <i>Web Fragment</i>: Web Item, Web Section, Web Panel, in all available JIRA locations. Web Fragments contain a unique location, making it easier to identify the right extension points for your add-on. 

**[Connect inspector](https://connect-inspector.atlassian.io/)**
The Connect inspector is an extremely useful tool allowing developers to watch live lifecycle and webhook events in your web browser. The inspector allows you to generate a temporary Atlassian Connect add-on you install into your Cloud Development Environment. It will live for three days and store any lifecycle and webhook events that it receives. 

**[JIRA cloud-to-server plugin converter](https://github.com/minhhai2209/jira-plugin-converter)**
This is a tool to convert an Atlassian Connect add-on's descriptor to a JIRA Server plugin. The remote server 
is shared between both. In this way, developers can build add-ons for JIRA Cloud first, then generate plugins 
for JIRA Server later.

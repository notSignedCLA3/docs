
## JAVA
<a href="https://bitbucket.org/atlassianlabs/atlassian-autowatch-plugin">JIRA Autowatcher</a> 
<br>
Automatically add watchers to newly created JIRAs.
<br><br>
<a href="https://bitbucket.org/atlassian/whoslooking-connect/overview">Who's Looking</a> 
<br>
Displays the users who are currently looking at a JIRA issue.

## Node.js
<a href="https://bitbucket.org/atlassianlabs/webhook-inspector">Webhook Inspector</a> 
<br>
Inspects the response bodies of the available webhooks in Atlassian Connect.
<br><br>
<a href="https://bitbucket.org/robertmassaioli/ep-tool">Entity Property Tool</a>
<br>
An Atlassian Connect add-on that allows manipulation of entity properties in JIRA.

## Haskell
<a href="https://bitbucket.org/atlassianlabs/my-reminders/overview">My Reminders</a>
Sends you reminders about JIRA issues

## Scala
<a href="https://bitbucket.org/atlassianlabs/atlas-lingo">Atlas Lingo</a>
<br>
Translate JIRA issues into other languages using Google Translate API.
<br><br>
<a href="https://bitbucket.org/atlassianlabs/whoslooking-connect-scala">Who's Looking</a>
<br>
Displays the users who are currently looking at a JIRA issue.

## Static Add-on
Static add-ons consist of entirely static content (HTML, CSS and JavaScript) and do not have a framework. 
<br>
<a href="https://bitbucket.org/atlassianlabs/atlassian-connect-whoslooking-connect-v2">Who's Looking</a>
<br>Displays the users who are currently looking at a JIRA issue.


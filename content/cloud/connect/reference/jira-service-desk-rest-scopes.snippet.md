# JIRA Service Desk REST scopes
For more information about the JIRA Service Desk REST APIs, please [refer to the documentation](https://developer.atlassian.com/jiradev/jira-apis).

{{< include path="docs/content/cloud/connect/reference/product-api-scopes.snippet.md" >}}

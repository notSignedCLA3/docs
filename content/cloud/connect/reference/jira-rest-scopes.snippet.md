# JIRA REST scopes
For more information about the JIRA REST APIs, please consult [the documentation on developer.atlassian.com](https://developer.atlassian.com/jiradev/jira-apis).

{{< include path="docs/content/cloud/connect/reference/product-api-scopes.snippet.md" >}}